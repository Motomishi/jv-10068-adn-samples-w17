﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Week2
{
    class Car : Vehicle
    {
        public void Drive()
        {
            Console.WriteLine("Car:Drive");
        }

        public override void Turn()
        {
            Console.WriteLine("Car:Turn");
        }

        public override void Maintain()
        {
            Console.WriteLine("Car:Maintain");
        }
    }
}
